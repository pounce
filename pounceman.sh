#!/bin/bash

DIALOG_CANCEL=1
DIALOG_ESC=255
DIALOG_EXTRA=3
pouncedir="/var/lib/pounce"

menu() {
  exec 3>&1
  selection=$(dialog \
    --backtitle "LibertaCasa IRC Services - Pounce Division - https://liberta.casa/" \
    --title "Welcome!" \
    --clear \
    --cancel-label "Exit" \
    --menu "Configure your IRC bouncer:" 0 0 8 \
    "1" "Edit active networks" \
    "2" "Edit disabled networks" \
    "3" "Add new network" \
    "6" "Manage client certificates" \
    2>&1 1>&3)
  exit_status=$?
  exec 3>&-
}
#    "4" "Enable network (deprecated)" \
#    "5" "Disable network (deprecated)" \

get_networks() {
  exec 3>&1
  if [ $pouncedir = "" ]; then
    exit
  fi
  pouncedir="/var/lib/pounce"
  #if ! $(find $pouncedir/users/$USER/$1 -mindepth 0 -maxdepth 0 -empty | grep -q .); then
  if [ "$(ls -A $pouncedir/users/$USER/$1)" ]; then
    COUNTER=1
    RADIOLIST=""
    user_choice=""
    for i in $pouncedir/users/$USER/$1/*; do
  	  FILENAME=$(basename "$i")
  	  RADIOLIST="$RADIOLIST $FILENAME $FILENAME off"
  	  let COUNTER=COUNTER+1
    done
    if [ $1 = "enabled" ]; then
	    user_choice=$(dialog --ok-label "Edit" \
                    --extra-button \
                    --extra-label "Disable" \
		    --backtitle "LibertaCasa IRC Services - Pounce Division - https://liberta.casa/" \
	  	  --radiolist "Networks" 0 0 $COUNTER \
  		  $RADIOLIST \
	    2>&1 1>&3)
            exit_status=$?
            case $exit_status in
              0) 
		if [ ! $user_choice = "" ]; then
		      edit_network "enabled"
		fi
              ;;
              3) set_network "enabled"
              ;;
           esac
    fi
    if [ $1 = "disabled" ]; then
	    user_choice=$(dialog --ok-label "Edit" \
                    --extra-button \
                    --extra-label "Enable" \
		    --backtitle "LibertaCasa IRC Services - Pounce Division - https://liberta.casa/" \
	  	  --radiolist "Networks" 0 0 $COUNTER \
  		  $RADIOLIST \
	    2>&1 1>&3)
            exit_status=$?
            case $exit_status in
              0)
		if [ ! $user_choice = "" ]; then
		      edit_network "disabled"
		fi
              ;;
              3) set_network "disabled"
              ;;
            esac
    fi
    exec 3>&-
    echo $user_choice > /tmp/userchoice
  else
    dialog --title "No entries found" \
	    --backtitle "LibertaCasa Pounce Configurator" \
	    --msgbox "There seem to be no $1 networks in your account." \
	    10 60
    user_choice=""
  fi
}

edit_network() {
  tmpfile=$(mktemp /tmp/$USER.XXXXXXXXXXXXXXXXXXXX)
  cp $pouncedir/users/$USER/$1/$user_choice $tmpfile
  nw_name=$user_choice
  nw_hostname=$(egrep '(^|\s)host =' $tmpfile | sed 's/host = //' - )
  nw_nickname=$(egrep '(^|\s)nick =' $tmpfile | sed 's/nick = //' - )
  nw_password=$(egrep '(^|\s)sasl-plain =' $tmpfile | sed 's/^sasl-plain = [^:]*://' - )
  nw_channels=$(egrep '(^|\s)join =' $tmpfile | sed 's/join = //' - )
  nw_away=$(egrep '(^|\s)away =' $tmpfile | sed 's/away = //' -)
  echo "$nw_hostname\n$nw_nickname\n$nw_password\n$nw_channels\n$nw_away" > /tmp/nwvars
  exec 3>&1
  user_input=$(dialog --ok-label "Save" \
	 --extra-button \
	 --extra-label "Delete" \
	 --backtitle "LibertaCasa IRC Services - Pounce Division - https://liberta.casa/" \
	 --title "Edit IRC Network" \
	 --form "$USER/$nw_name:" \
  15 60 0 \
	 "Hostname:"       1 1 "$nw_hostname"	1 18 33 0 \
	 "Nickname:"       2 1 "$nw_nickname"	2 18 33 0 \
	 "SASL Password:"  3 1 "$nw_password"	3 18 33 128 \
	 "Channels:"	   4 1 "$nw_channels"	4 18 33 0 \
	 "Away message:"   5 1 "$nw_away"	5 18 33 0 \
  2>&1 1>&3)
  exit_status=$?
  exec 3>&-
  echo $user_input > /tmp/userinput
  case $exit_status in
          0) 
	     #tmp_name=$(echo "$user_input" | sed -n 1p)
	     tmp_hostname=$(echo "$user_input" | sed -n 1p)
	     tmp_nickname=$(echo "$user_input" | sed -n 2p)
	     tmp_password=$(echo "$user_input" | sed -n 3p)
	     tmp_channels=$(echo "$user_input" | sed -n 4p)
	     tmp_away=$(echo "$user_input" | sed -n 5p)
	     echo "$tmp_name\n$tmp_nickname\n$tmp_password\n$tmp_channels\n$tmp_away" > /tmp/tmpvars
	     sed -e "s/$(egrep '(^|\s)host =' $tmpfile)/host = $tmp_hostname/" -i $tmpfile
	     sed -e "s/$(egrep '(^|\s)nick =' $tmpfile)/nick = $tmp_nickname/" -i $tmpfile
	     sed -e "s+$(egrep '(^|\s)real =' $tmpfile)+real = $tmp_nickname - https://liberta.casa/+" -i $tmpfile
	     sed -e "s/$(egrep '(^|\s)user =' $tmpfile)/user = $tmp_nickname/" -i $tmpfile
	     sed -e "s/$(egrep '(^|\s)sasl-plain =' $tmpfile)/sasl-plain = $tmp_nickname:$tmp_password/" -i $tmpfile
	     sed -e "s/$(egrep '(^|\s)join =' $tmpfile)/join = $tmp_channels/" -i $tmpfile
	     sed -e "s+$(egrep '(^|\s)away =' $tmpfile)+away = $tmp_away+" -i $tmpfile
	     DIFF=$(diff $pouncedir/users/$USER/$1/$user_choice $tmpfile)
	     if [ ! "$DIFF" = "" ]; then
	       cp $tmpfile $pouncedir/users/$USER/$1/$user_choice
	     fi
	     rm $tmpfile
	  ;;
          3)
             remove_network
          ;;
  esac
  nw_name=""
  nw_hostname=""
  nw_nickname=""
  nw_password=""
  nw_channels=""
  nw_away=""
  tmp_name=""
  tmp_hostname=""
  tmp_nickname=""
  tmp_password=""
  tmp_channels=""
  tmp_away=""
  tmpfile=""
  DIFF=""
}

set_network() {
  if [[ $1 = "disabled" ]] && [[ $user_choice != "" ]]; then
    mv $pouncedir/users/$USER/disabled/$user_choice $pouncedir/users/$USER/enabled/$user_choice
    systemctl --user enable --now pounce@$user_choice
  fi
  if [[ $1 = "enabled" ]] && [[ $user_choice != "" ]]; then
    systemctl --user disable --now pounce@$user_choice
    mv $pouncedir/users/$USER/enabled/$user_choice $pouncedir/users/$USER/disabled/$user_choice
  fi
  $user_choice=""
}

remove_network() {
  dialog --title "Delete network" \
	  --backtitle "LibertaCasa Pounce Configurator" \
	  --yesno "This will permanently delete the network configuration $USER/$user_choice - Are you sure?" 7 60
  exit_status=$?
  case $exit_status in
	  0) rm -f $(find $pouncedir/users/$USER -type f -name "$user_choice");;
  esac
}

add_network() {
  tmp_name=""
  tmp_hostname=""
  tmp_nickname=""
  tmp_password=""
  tmp_channels=""
  tmp_away=""
  initnew=""
  while [[ -z $tmp_name || -z $tmp_hostname || -z $tmp_nickname || -z $tmp_password || -z $tmp_channels || -z $tmp_away ]]; do
    exec 3>&1
    user_input=$(dialog --ok-label "Submit" \
  	 --backtitle "LibertaCasa IRC Services - Pounce Division - https://liberta.casa/" \
  	 --title "New IRC Network" \
  	 --form "Network specific details:" \
    20 50 0 \
  	 "Arbitrary name:" 1 1 "$tmp_name"	1 18 33 0 \
  	 "Hostname:"       2 1 "$tmp_hostname"	2 18 33 0 \
  	 "Nickname:"       3 1 "$tmp_nickname"	3 18 33 0 \
  	 "SASL Password:"  4 1 "$tmp_password"	4 18 33 128 \
  	 "Channels:"	   5 1 "$tmp_channels"	5 18 33 0 \
  	 "Away message:"   6 1 "$tmp_away"	6 18 33 0 \
    2>&1 1>&3)
    #3>&1 1>&2 2>&3 3>&-)
    if [ "$?" = "1" ]; then
	    initnew="false"
	    break
    fi
    exec 3>&-
    echo $user_input > /tmp/userinput
    tmp_name=$(echo "$user_input" | sed -n 1p)
    tmp_hostname=$(echo "$user_input" | sed -n 2p)
    tmp_nickname=$(echo "$user_input" | sed -n 3p)
    tmp_password=$(echo "$user_input" | sed -n 4p)
    tmp_channels=$(echo "$user_input" | sed -n 5p)
    tmp_away=$(echo "$user_input" | sed -n 6p)
  done
  if [[ $tmp_name != "TEMPLATE" ]] && [[ $initnew != "false" ]]; then
    cp $pouncedir/TEMPLATE $pouncedir/users/$USER/disabled/$tmp_name
    sed -e "s+%%POUNCEDIR%%+$pouncedir+" -i $pouncedir/users/$USER/disabled/$tmp_name
    sed -e "s/%%USER%%/$USER/" -i $pouncedir/users/$USER/disabled/$tmp_name
    sed -e "s/%%NAME%%/$tmp_name/" -i $pouncedir/users/$USER/disabled/$tmp_name
    sed -e "s:%%PASSWORD%%:$tmp_password:" -i $pouncedir/users/$USER/disabled/$tmp_name
    sed -e "s/%%HOSTNAME%%/$tmp_hostname/" -i $pouncedir/users/$USER/disabled/$tmp_name
    sed -e "s/%%CHANNELS%%/$tmp_channels/" -i $pouncedir/users/$USER/disabled/$tmp_name
    sed -e "s/%%USERNAME%%/$tmp_nickname/" -i $pouncedir/users/$USER/disabled/$tmp_name
    sed -e "s/%%AWAY%%/$tmp_away/" -i $pouncedir/users/$USER/disabled/$tmp_name
 fi
}

certman() {
 exec 3>&1
 certmansel=$(dialog \
  --backtitle "LibertaCasa IRC Services - Pounce Division - https://liberta.casa/" \
  --title "Certificate Manager" \
  --clear \
  --cancel-label "Exit" \
  --menu "" 0 0 8 \
  "1" "Manage Certificate Authority" \
  "2" "Manage Client Certificates" \
  "3" "Create new client certificate" \
   2>&1 1>&3)
 exec 3>&-
 case $certmansel in
   1 )
     get_ca
     ;;
   2 )
     get_certs
     ;;
   3 )
     add_cert
     ;;
 esac
}

get_ca() {
  sha1=$(openssl x509 -noout -fingerprint -sha1 -in $pouncedir/users/$USER/ca.pem)
  sha256=$(openssl x509 -noout -fingerprint -sha256 -in $pouncedir/users/$USER/ca.pem)
#  exec 3>&1
  dialog \
    --backtitle "LibertaCasa IRC Services - Pounce Division - https://liberta.casa/" \
    --title "Fingerprints" \
    --extra-button \
    --extra-label "New CA" \
    --msgbox "$sha1\n\n$sha256" 10 90
  exit_status=$?
  case $exit_status in
    0 )
      certman
      ;;
    3 )
      new_ca
      ;;
  esac
#  exec 3>&1-
}

new_ca() {
  dialog --title "Re-generate Certificate Authority" \
	  --backtitle "LibertaCasa Pounce Configurator" \
          --yes-label "Proceed" \
          --no-label "Cancel" \
	  --yesno "!WARNING!\nThis will REMOVE your existing CA certificate and generate a new one.\nALL existing client certificates will be INVALIDATED immediately.\nYou will need to generate and deploy new client certificates if you decide to continue." 10 90
  exit_status=$?
  case $exit_status in
	  0 )
            rm -f $pouncedir/users/$USER/ca.pem
            /usr/local/bin/pounce -g $pouncedir/users/$USER/ca.pem
            get_ca
            ;;
          1 )
            get_ca
            ;;
  esac
}

get_certs() {
  exec 3>&1
  #if ! $(find $pouncedir/users/$USER/certs -mindepth 0 -maxdepth 0 -empty -type f | grep -q -); then
  if [ "$(ls -A $pouncedir/users/$USER/certs)" ]; then
    CRTCOUNTER=1
    CRTRADIOLIST=""
    crt_choice=""
    for crt in $pouncedir/users/$USER/certs/*; do
  	  CRTFILENAME=$(basename "$crt")
  	  CRTRADIOLIST="$CRTRADIOLIST $CRTFILENAME $CRTFILENAME off"
  	  let CRTCOUNTER=CRTCOUNTER+1
    done
    crt_choice=$(dialog --ok-label "View" \
            --cancel-label "Back" \
            --extra-button \
            --extra-label "Delete" \
            --title "Client Certificates" \
	    --backtitle "LibertaCasa IRC Services - Pounce Division - https://liberta.casa/" \
  	  --radiolist "Manage your client certificates:" 0 0 $CRTCOUNTER \
	  $CRTRADIOLIST \
    2>&1 1>&3)
    exit_status=$?
    case $exit_status in
      0)
	if [ ! $crt_choice = "" ]; then
	  get_cert "$crt_choice"
	else
	  get_certs
	fi
      ;;
      1) certman
      ;;
      3)
	if [ ! $crt_choice = "" ]; then
	  remove_cert "$crt_choice"
	else
	  get_certs
	fi
      ;;
   esac
    exec 3>&-
    echo $crt_choice > /tmp/crtchoice
  else
    dialog --title "No certificates found" \
	    --backtitle "LibertaCasa Pounce Configurator" \
	    --yesno "There seem to be no client certificates in your account. Create your first one?" \
	    10 60
    exit_status=$?
    case $exit_status in
            0 )
              add_cert
              ;;
            #1 )
            #  get_certs
            #  ;;
    esac
    crt_choice=""
  fi
  exec 3>&-
}

get_cert() {
  cert=$1
  sha1=$(openssl x509 -noout -fingerprint -sha1 -in $pouncedir/users/$USER/certs/$cert)
  sha256=$(openssl x509 -noout -fingerprint -sha256 -in $pouncedir/users/$USER/certs/$cert)
  dialog \
    --backtitle "LibertaCasa IRC Services - Pounce Division - https://liberta.casa/" \
    --title "Fingerprints" \
    --ok-label "Back" \
    --extra-button \
    --extra-label "Download" \
    --msgbox "$sha1\n\n$sha256" 10 90
  exit_status=$?
  case $exit_status in
    0 )
      get_certs
      ;;
    3 )
      if [ ! $cert = "" ]; then
	      export_cert "$cert"
      fi
      ;;
  esac
}

export_cert() {
  cert=$1
  exec 3>&1
  pbincli send -f $pouncedir/users/$USER/certs/$cert -B -q | grep -o "https://.*"  | dialog \
	  --backtitle "LibertaCasa IRC Services - Pounce Division - https://liberta.casa/" \
	  --title "Certificate Download" \
	  --programbox "One-time download for client certificate $cert:" \
	  5 150
  exit_status=$?
  case $exit_status in
    0 )
       get_certs
       ;;
  esac
  exec 3>&-
}

add_cert() {
  exec 3>&1
  crt_input=$(dialog --ok-label "Submit" \
	 --backtitle "LibertaCasa IRC Services - Pounce Division - https://liberta.casa/" \
	 --title "New Client Certificate" \
	 --inputbox "Certificate name:" \
	 10 60 \
  2>&1 1>&3)
  echo $crt_input > /tmp/crtinput
  case $crt_input in
    "")
       dialog --title "Error" \
              --backtitle "LibertaCasa Pounce Configurator" \
              --msgbox "Did you supply a name for the new certificate?" \
              10 60
       ;;
    *)
       /usr/local/bin/pounce -a $pouncedir/users/$USER/certs/ca.pem -g $pouncedir/users/$USER/certs/$crt_input.pem
       ;;
    esac
  crt_input = ""
  exec 3>&-
  get_certs
}

remove_cert() {
  dialog --title "Delete Certificate" \
	  --backtitle "LibertaCasa Pounce Configurator" \
	  --yesno "This will permanently delete the client certificate $USER/$1.\nThis will BREAK all IRC connections utilizing it.\nAre you sure?" 10 50
  exit_status=$?
  case $exit_status in
	  0)
            rm -f $pouncedir/users/$USER/certs/$1
            get_certs
            ;;
          1)
            get_certs
            ;;
  esac
}

while true; do
  menu
  case $exit_status in
    $DIALOG_CANCEL)
      clear
      echo "Good bye!"
      exit
      ;;
    $DIALOG_ESC)
      clear
      echo "See you!" >&2
      exit 1
      ;;
  esac
  case $selection in
    1 )
      get_networks "enabled"
#      if [ ! -z "$user_choice" ]; then
#	      edit_network "enabled"
#	      if [ $exit_status = $DIALOG_EXTRA ]; then
#	              remove_network
#	      fi
#      fi
      ;;
    2 )
      get_networks "disabled"
#      if [ ! -z "$user_choice" ]; then
#	      edit_network "disabled"
#	      if [ $exit_status = $DIALOG_EXTRA ]; then
#	              remove_network
#	      fi
#      fi
      ;;
    3 )
      add_network
      ;;
#    4 )
#      get_networks "disabled"
#      if [ ! -z "$user_choice" ]; then
#	      set_network "disabled"
#      fi
#      ;;
#    5 )
#      get_networks "enabled"
#      if [ ! -z "$user_choice" ]; then
#	      set_network "enabled"
#      fi
#      ;;
    6 )
      certman
      ;;
  esac
done
