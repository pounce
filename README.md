Public IRC bouncer powered by https://git.causal.agency/pounce/.
Intends to provide a less bloated, more IRCv3 friendly and TUI manageable alternative to ZNC.

Still in development.
